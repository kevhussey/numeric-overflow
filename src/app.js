'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json({limit: "100kb"}));

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main
app.get('/', (req, res) => {
    try {
        if (approval(req.query.amount)) {
            res.status(400).end(req.query.amount + ' requires approval.');
        } else {
            res.status(200).end(req.query.amount + ' does not require approval.');
        }
    } catch(err) {
        res.status(400).end('bad request');
    }

});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {

    if (value === undefined || value === null || value === "") {
        throw RangeError("null or empty");
    }

    if (typeof value !== "string" && typeof value !== "number") {
        throw RangeError("must be numeric");
    }

    const parsedValue = parseInt(value);
    if (isNaN(parsedValue)) {
        throw new RangeError("NAN");
    }

    console.log(`parsedValue  ${parsedValue}`);
    var threashold = 1000;
    var surcharge = 10;

    var amount = Int32Array.from([parsedValue],x=>parseInt(x+surcharge));

    if (amount[0] <0 || additionDoesOverflow(parsedValue, surcharge, amount[0]) ) {
        throw new RangeError();
    }
    if (amount[0] > 0 && amount[0] >= threashold) {
        return true;
    };
    return false;
};

function additionDoesOverflow(a, b, c) {
    return a !== c-b || b !== c-a;
}

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval };
